import Row from "./Row";
import Cell from "./Cell";

const Table = ({ items }) => {
  let i = 0;
  let keysArr = [];
  items.forEach((item) => {
    if (i === 0) {
      i++;
      keysArr = Object.keys(item);
    }
  });
  //   console.log(keysArr);
  return (
    <div className="table-container">
      <table>
        <thead>
          <tr>
            {keysArr.map((item) => (
              <Cell key={item} cellData={item} />
            ))}
          </tr>
        </thead>
        <tbody>
          {items.map((item) => (
            <Row key={item.id} item={item} />
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
